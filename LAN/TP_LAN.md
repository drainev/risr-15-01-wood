# TP RISR15-01 LAN
## SOMMAIRE
* [0. Schéma de base](#0-schema-de-base)
* [I. Nommage des équipements](#i-nommage-des-equipements)
* [II. Adressage IP](#ii-adressage-ip)
* [III. Configuration basique des equipeemnts réseau](#iii-configuration-basique-des-equipements-reseau)
* [IV. Configuration des VLANs](#iv-configuration-des-vlans)
* [V. VTP](#v-vtp)
* [VI. Le mode Access](#vi-le-mode-access)
* [VII. Le mode Trunk](#vii-le-mode-trunk)
* [VIII. Liaison d'agrégats (ETHERCHANNEL)](#viii-liaison-dagrégats-etherchannel)
* [IX. Spanning-Tree](#ix-spanning-tree)
* [X. Routage InterVlan](#x-routage-intervlan)

## 0. Schéma de base

![](https://i.imgur.com/lIIjiG8.png)

## I. Nommage des équipements
|  Nom   | Equipement            |
|:------:| --------------------- |
|   R1   | Routeur 2911          |
|   R2   | Routeur 2911          |
|  SW1   | Switch 2960           |
|  SW2   | Switch 2960           |
|  SW3   | Switch 3560 (L3)      |
| SERVER | VM (WServer au choix) |
| CLIENT | VM (WClient au choix) |

## II. Adressage IP

### 1. Vlans
| Vlan ID | Nom Vlan | Adresse Réseau |
|---------|----------|----------------|
| 1 | Management | 10.1.1.0/24 |
| 10 | Server | 10.1.10.0/24 |
| 20 | Clients | 10.1.20.0/24 |

*Bonnes pratiques :
Changer l’ID du VLAN de gestion dans un autre VLAN que le VLAN 1.
Changer l’ID du VLAN natif dans un autre VLAN que le VLAN 1.*

### 2. Interfaces
*Interface L3 = Niveau 3 : Interface de routage donc pas de VLAN !*
*SubIf = Sous-interface Vlan : Sert au routage interVlan sur un Router*
*HSRP = Hot Standby Router Protocol : Protocole HA (propriétaire Cisco) pour les routeurs*

| Nom | VLAN | Adresse IP |
| -------- | -------- | -------- |
| R1 (Vers R2) | Interface L3 | 200.10.10.1/24 |
| R1 (Vers SW3) | Interface L3 | 192.168.0.1/30 |
| R1 SubIf Vlan 1 (Vers SW1) | Interface L3 | 10.1.1.252 |
| R1 SubIf Vlan 10 (Vers SW1) | Interface L3 | 10.1.10.252 |
| R1 SubIf Vlan 20 (Vers SW1) | Interface L3 | 10.1.20.252 |
| R2 (Vers R1) | Interface L3 | 200.10.10.2/24 |
| R2 (Vers SRV-WEB) | Interface L3 | 8.8.8.254/24 |
| SW3 (vers R1) | Interface L3 | 192.168.0.2/30 |
| SW3 Interface Vlan 1 | Interface L3 | 10.1.1.253/24 |
| SW3 Interface Vlan 10 | Interface L3 | 10.1.10.253/24 |
| SW3 Interface Vlan 20 | Interface L3 | 10.1.20.253/24 |
| SW1 Interface Vlan 1 | Interface L3 | 10.1.1.1/24 |
| SW2 Interface Vlan 1 | Interface L3 | 10.1.1.2/24 |
| HSRP Vlan 1 | Interface L3 | 10.1.1.254/24 |
| HSRP Vlan 10 | Interface L3 | 10.1.10.254/24 |
| HSRP Vlan 20 | Interface L3 | 10.1.20.254/24 |
| SRV-WEB | N/A | 8.8.8.1/24 |
| SERVER | 10 | 10.1.10.1/24 |
| CLIENT | 20 | 10.1.20.1/24 |


## III. CONFIGURATION BASIQUE DES EQUIPEMENTS RESEAU

### 1. Nom
```=
Switch#conf t
Switch(config)#hostname SW1
SW1(config)#
```

### 2. Mot de passe enable + Encryption des mots de passes
```=
SW1(config)#enable secret cisco
```

Vérification :
```=
SW1#sh run
Building configuration...

Current configuration : 1150 bytes
!
version 15.0
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname SW1
!
enable secret 5 $1$mERr$hx5rVt7rPNoS4wqbXKX7m0
```

### 3. Compte administrateur local
```=
SW1(config)#username admin privilege 15 password cisco
```

### 4. Chiffrement du mdp (actuel et futurs)
```=
SW1(config)#service password-encryption
```

### 5. Verification des privilèges
```=
SW1#show privilege
Current privilege level is 15
```

### 6. SSH
Source : [https://reussirsonccna.fr/configuration-du-ssh-sur-ios/](https://reussirsonccna.fr/configuration-du-ssh-sur-ios/)
#### A. Définition d'un nom de domaine
```=
SW1(config)#ip domain-name labo.lan
```

#### B Génération de la clé de chiffrement
```=
SW1(config)#crypto key generate rsa
The name for the keys will be: SW1.labo.lan
Choose the size of the key modulus in the range of 360 to 2048 for your
  General Purpose Keys. Choosing a key modulus greater than 512 may take
  a few minutes.

How many bits in the modulus [512]: 1024
% Generating 1024 bit RSA keys, keys will be non-exportable...[OK]
```

#### C. Activation SSH
```=
SW1(config)#line vty 0 4
*Mar 1 0:32:13.181: %SSH-5-ENABLED: SSH 1.99 has been enabled
SW1(config-line)#transport input ssh
SW1(config-line)#login local
SW1(config-line)#exit
```

#### D. Description des interfaces

Afin de facilement retrouver les autres équipements avec la commande ```sh run``` (```show running-config```), il est de coutûme d'inscrire une description sur les interfaces de connexions.

Grâce au protocole de découverte de Cisco (CDP : Cisco Discovery Protocol), rien de plus simple. Grâce à la commande ```sh cdp neighbors```

```=
SW3#sh cdp neighbors 
Capability Codes: R - Router, T - Trans Bridge, B - Source Route Bridge
                  S - Switch, H - Host, I - IGMP, r - Repeater, P - Phone
Device ID    Local Intrfce   Holdtme    Capability   Platform    Port ID
SW2          Fas 0/2          137            S       2960        Fas 0/1
SW1          Fas 0/1          167            S       2960        Fas 0/1
```

On peut donc constater que SW1 est branché sur le port Fast-Ethernet 0/1 et SW2 sur Fas 0/2. Il reste alors à y ajouter une description.

```=
SW3(config)#interface fastEthernet 0/1
SW3(config-if)#description "Vers SW1"
SW3(config-if)#interface Fa 0/2
SW3(config-if)#description "Vers SW2"
```

Vérification :
Lancer un "*sh run*"
```=
interface FastEthernet0/1
 description "Vers SW1"
!
interface FastEthernet0/2
 description "Vers SW2"
```

## IV. Configuration des VLAN
> Source : https://cisco.goffinet.org/ccna/vlans/configuration-vlan-cisco-ios/#1-vlan-et-param%C3%A8tres-switchport-par-d%C3%A9faut
> 
### 0. Préambule

Par défaut, un port physique d’un commutateur Cisco est un “switchport”, un port de commutation.

Il est configuré par défaut dans le mode “dynamic auto” mais il est en mode opérationnel “access”. Il est associé au VLAN 1 (default).

S’il devait être monté trunk, le VLAN natif serait le VLAN 1 et l’encapsulation “Trunk” serait négociée par DTP.

Un port dynamique est un port qui restera un port “access” ou qui se montera en port “trunk” en fonction des messages Dynamic Trunk Protocol (DTP, protocole propriétaire Cisco) reçus par l’interface.

### 1. VLAN et interfaces SVI

#### A. Création des VLANs

En temps normal, les VLANs doivent être créés sur chaque site. Mais dans cet exemple, nous mettrons en place le VTP, donc les créer sur un seul sera suffisant.

```=
SW1(config)#vlan 10
SW1(config-vlan)#name "Server"
SW1(config-vlan)#vlan 20
SW1(config-vlan)#name "Clients"
SW1(config-vlan)#exit
```

Vérification :
```=
SW1#sh vlan

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Fa0/1, Fa0/2, Fa0/3, Fa0/4
                                                Fa0/5, Fa0/6, Fa0/7, Fa0/8
                                                Fa0/9, Fa0/10, Fa0/11, Fa0/12
                                                Fa0/13, Fa0/14, Fa0/15, Fa0/16
                                                Fa0/17, Fa0/18, Fa0/19, Fa0/20
                                                Fa0/21, Fa0/22, Fa0/23, Fa0/24
                                                Gig0/1, Gig0/2
10   Server                           active    
20   Clients                          active    
1002 fddi-default                     active    
1003 token-ring-default               active    
1004 fddinet-default                  active    
1005 trnet-default                    active    

VLAN Type  SAID       MTU   Parent RingNo BridgeNo Stp  BrdgMode Trans1 Trans2
---- ----- ---------- ----- ------ ------ -------- ---- -------- ------ ------
1    enet  100001     1500  -      -      -        -    -        0      0
10   enet  100010     1500  -      -      -        -    -        0      0
20   enet  100020     1500  -      -      -        -    -        0      0
1002 fddi  101002     1500  -      -      -        -    -        0      0   
1003 tr    101003     1500  -      -      -        -    -        0      0   
1004 fdnet 101004     1500  -      -      -        ieee -        0      0   
1005 trnet 101005     1500  -      -      -        ibm  -        0      0   

VLAN Type  SAID       MTU   Parent RingNo BridgeNo Stp  BrdgMode Trans1 Trans2
---- ----- ---------- ----- ------ ------ -------- ---- -------- ------ ------

Remote SPAN VLANs
------------------------------------------------------------------------------

Primary Secondary Type              Ports
------ ---------- ----------------- ------------------------------------------
```

#### B. L’interface VLANx de gestion (SVI)

L’interface VLANx de gestion (SVI) correspond aux interfaces physiques appartenant à ce VLAN pour le joindre en IPv4 à des fins de gestion (en Telnet, SSH, HTTP ou SNMP).

Comme tout périphérique joignable sur le réseau, le commutateur doit posséder une adresse IPv4, un masque et une passerelle.

*nb : Sur un équipement réseau, les interfaces de niveaux 3 sont toujours "shutdown" par défaut.*

##### Vlan 1
```=
SW3(config)#interface vlan 1
SW3(config-if)#description "Management"
SW3(config-if)#ip address 10.1.1.253 255.255.255.0
SW3(config-if)#no shut

%LINK-5-CHANGED: Interface Vlan1, changed state to up
%LINEPROTO-5-UPDOWN: Line protocol on Interface Vlan1, changed state to up
%IP-4-DUPADDR: Duplicate address 10.1.1.253 on Vlan1, sourced by 0090.2186.09E8

SW3(config-if)#exit
SW3(config)#ip default-gateway 10.1.1.254
```

##### Vlan 10
```=
SW3(config)#interface vlan 10
SW3(config-if)#description "Servers"
%LINK-5-CHANGED: Interface Vlan10, changed state to up

SW3(config-if)#ip address 10.1.10.253 255.255.255.0
SW3(config-if)#no shut
SW3(config-if)#exit
SW3(config)#ip default-gateway 10.1.10.254
```

##### Vlan 20
```=
SW3(config)#interface vlan20
%LINK-5-CHANGED: Interface Vlan20, changed state to up
SW3(config-if)#description "Clients"
SW3(config-if)#ip address 10.1.20.253 255.255.255.0
SW3(config-if)#no shut
SW3(config-if)#exit
SW3(config)#ip default-gateway 10.1.20.254
```

Vérification :

```=
SW3#sh ip interface brief
Interface              IP-Address      OK? Method Status                Protocol 
Vlan1                  10.1.1.253      YES manual up                    up 
Vlan10                 10.1.10.253     YES manual up                    down 
Vlan20                 10.1.20.253     YES manual up                    down
```

Mais la colonne “Protocol” reste “down” pour les Vlan 10 et 20 ce qui signifie un problème de couche 2 (L2).

## V. VTP

### 0. Préambule

VTP (Virtual Trunking Protocol) est un protocole propriétaire Cisco servant à maintenir la base de données de VLANs sur plusieurs commutateurs de manière cohérente.

Deux éléments sont nécessaires au bon fonctionnement de VTP :

* Définir un nom de domaine VTP (appelé aussi domaine de gestion). Ne participent à cette gestion que les switches qui appartiennent à un même domaine.

* Définir pour chaque commutateur un rôle :
  * soit client,
  * soit transparent,
  * soit, pour un seul d’entre eux, server.

**Il ne peut y avoir qu’un seul switch “server” dans un domaine VTP.**

### 1. Voir le status VTP

```=
SW3#sh vtp status
VTP Version capable             : 1 to 2
VTP version running             : 2
VTP Domain Name                 : 
VTP Pruning Mode                : Disabled
VTP Traps Generation            : Disabled
Device ID                       : 000A.F35E.C000
Configuration last modified by 0.0.0.0 at 3-1-93 00:11:04
Local updater ID is 10.1.1.253 on interface Vl1 (lowest numbered VLAN interface found)

Feature VLAN : 
--------------
VTP Operating Mode                : Server
Maximum VLANs supported locally   : 1005
Number of existing VLANs          : 7
Configuration Revision            : 4
MD5 digest                        : 0x2C 0x96 0x55 0x42 0xC9 0x50 0x28 0x6B 
                                    0x8C 0xAB 0xF0 0x3A 0x58 0xF2 0x53 0x3E
```
### 2. Domaine VTP Cisco

```=
SW3(config)#vtp domain labo.lan
Changing VTP domain name from NULL to labo.lan
```

### 3. Configuration du mdp VTP

```=
SW3(config)#vtp password cisco123
Setting device VLAN database password to cisco123
```


### 4. Définition du serveur VTP

***Rappel : Il ne peut y avoir qu'un et un seul serveur VTP sur le réseau.***
```=
SW3(config)#vtp mode server
Device mode already VTP SERVER.
```
On remarque que le switch est déjà en mode Server par défaut, ce qui est pratique car on peut créer des VLANs une fois le switch sorti du carton.


### 5. Configuration des clients VTP

Le mieux est de basculer en mode "T"ransparent" pour mettre la valeur de révision de la configuration à zéro avant de le rebasculer en mode "Client".

```=
SW1(config)#vtp mode transparent 
Setting device to VTP TRANSPARENT mode.
SW1(config)#vtp mode client 
Setting device to VTP CLIENT mode.
```

On peut voir que la valeur de "Configuration Revision" est bien tombée à 0.

```=
SW1(config)#do sh vtp status
VTP Version capable             : 1 to 2
VTP version running             : 2
VTP Domain Name                 : labo.lan
VTP Pruning Mode                : Disabled
VTP Traps Generation            : Disabled
Device ID                       : 000D.BDE2.2300
Configuration last modified by 10.1.1.1 at 3-1-93 03:05:14

Feature VLAN : 
--------------
VTP Operating Mode                : Client
Maximum VLANs supported locally   : 255
Number of existing VLANs          : 7
Configuration Revision            : 0
MD5 digest                        : 0x7F 0xD1 0x9A 0xAE 0x40 0x30 0x04 0xB5 
                                    0xAA 0x08 0x39 0x84 0x5D 0x9C 0x3F 0x2C
```

### 6. Vérification de la distribution des VLANs

Sur les clients VTP :
On peut constater qu'il a récupérer les VLANs
```=
SW1(config)#do sh vtp status
VTP Version capable             : 1 to 2
VTP version running             : 2
VTP Domain Name                 : labo.lan
VTP Pruning Mode                : Disabled
VTP Traps Generation            : Disabled
Device ID                       : 000D.BDE2.2300
Configuration last modified by 10.1.1.1 at 3-1-93 03:05:14

Feature VLAN : 
--------------
VTP Operating Mode                : Client
Maximum VLANs supported locally   : 255
Number of existing VLANs          : 7
Configuration Revision            : 0
MD5 digest                        : 0x7F 0xD1 0x9A 0xAE 0x40 0x30 0x04 0xB5 
                                    0xAA 0x08 0x39 0x84 0x5D 0x9C 0x3F 0x2C
```


## VI. Le mode Access

Sous Cisco IOS un port Access est un port qui appartient à un seul VLAN. En option, on peut activer “spanning-tree portfast” qui fait passer le port directement à l’état STP “forwarding” vu qu’il connectera des périphériques de terminaison (qui ne créent pas de boucles).

### 1. Configuration

Dans cet exemple, je souhaite que les ports Fa 0/5 à 0/20 soient dans le VLAN 20 (Clients) et les ports de Fa 0/21 à Fa 0/24 dans le VLAN 10 (Servers). Les ports Fa 0/1 à 0/4 étant dans le VLAN par défaut, le VLAN de management. (Mauvaise pratique ! Le mieux est de ne rien laisser dans le VLAN par défaut)

```=
SW2(config)#interface range  fastEthernet 0/5 - 20
SW2(config-if-range)#switchport mode access
SW2(config-if-range)#switchport access vlan 20
SW2(config-if-range)#exit
SW2(config)#interface range fastEthernet 0/21 - 24
SW2(config-if-range)#switchport mode access 
SW2(config-if-range)#switchport access vlan 10
SW2(config-if-range)#exit
```

### 2. Vérification

On vérifie que les ports soient bien dans les bons VLANs.

```=
SW2#sh vlan

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Fa0/1, Fa0/2, Fa0/3, Fa0/4
                                                Gig0/1, Gig0/2
10   Servers                          active    Fa0/21, Fa0/22, Fa0/23, Fa0/24
20   Clients                          active    Fa0/5, Fa0/6, Fa0/7, Fa0/8
                                                Fa0/9, Fa0/10, Fa0/11, Fa0/12
                                                Fa0/13, Fa0/14, Fa0/15, Fa0/16
                                                Fa0/17, Fa0/18, Fa0/19, Fa0/20
```

## VII. Le mode Trunk

### 1. Configuration d'un port "Trunk"

Un port dit “Trunk” est un port qui transporte le trafic appartenant à plusieurs VLANs.

```=
SW3(config-if)#switchport trunk encapsulation dot1q
SW3(config-if)#switchport mode trunk

SW3(config-if)#
%LINEPROTO-5-UPDOWN: Line protocol on Interface FastEthernet0/1, changed state to down

%LINEPROTO-5-UPDOWN: Line protocol on Interface FastEthernet0/1, changed state to up

%LINEPROTO-5-UPDOWN: Line protocol on Interface Vlan10, changed state to up

%LINEPROTO-5-UPDOWN: Line protocol on Interface Vlan20, changed state to up
```

### 2. Restrictions sur le Trunk

```=
SW3(config)#interface fa 0/1
SW3(config-if)#switchport trunk allowed vlan 1,10,20
```

### 3. Vérification

```=
SW3#sh interfaces trunk 
Port        Mode         Encapsulation  Status        Native vlan
Fa0/1       on           802.1q         trunking      1

Port        Vlans allowed on trunk
Fa0/1       1,10,20

Port        Vlans allowed and active in management domain
Fa0/1       1,10,20

Port        Vlans in spanning tree forwarding state and not pruned
Fa0/1       10,20
```

## VIII. Liaison d'agrégats (ETHERCHANNEL)

### 0. Préambule
Le protocole LACP permet la mise en place d’agrégat de liens qui permet de regrouper plusieurs liens physiques en un seul lien logique et ainsi améliorer les performances en termes de bande-passante, de haute disponibilité et de répartition de charge.

LACP signifie Link Aggregation Control Protocol est un protocole de niveau 2 qui a pour référence IEEE "802.3ad".

### 1. Configuration LACP

Pour vérifier quel interface inclure dans l'agrégat :

```=
SW1#sh cdp neighbors 
Capability Codes: R - Router, T - Trans Bridge, B - Source Route Bridge
                  S - Switch, H - Host, I - IGMP, r - Repeater, P - Phone
Device ID    Local Intrfce   Holdtme    Capability   Platform    Port ID
SW2          Gig 0/2          167            S       2960        Gig 0/1
SW2          Gig 0/1          167            S       2960        Gig 0/2
SW3          Fas 0/1          121                    3560        Fas 0/1
```

On peut voir les deux liens qui vont vers SW2, Gig 0/1 & 0/2.

```=
SW1(config)#interface range GigabitEthernet 0/1 - 2
SW1(config-if-range)#channel-group 1 mode active
Creating a port-channel interface Port-channel 1

%LINEPROTO-5-UPDOWN: Line protocol on Interface GigabitEthernet0/1, changed state to down
%LINEPROTO-5-UPDOWN: Line protocol on Interface GigabitEthernet0/1, changed state to up
%LINEPROTO-5-UPDOWN: Line protocol on Interface GigabitEthernet0/2, changed state to down
%LINEPROTO-5-UPDOWN: Line protocol on Interface GigabitEthernet0/2, changed state to up
```

> Note : A la place du mode "active" il est possible d'indiquer le mode "passive" pour le second switch, il sera alors en attente que le switch à l'autre extrémité lui envoie un signal lui indiqué qu'il est actif en LACP. Pour que l'EtherChannel fonctionne avec LACP, il faut être active/active ou active/passive.

### 2. Vérification

Dans les résultats de ```sh run```, une nouvelle interface apparaît.
On peut également remarquer une nouvelle ligne en dessous des interfaces que nous venons de configurer.

N'oublions pas de configurer cette nouvelle interface en [mode trunk](#7-le-mode-trunk).

```=
interface Port-channel1
 switchport mode trunk

interface GigabitEthernet0/1
 description "Vers SW2 - LACP"
 switchport mode trunk
 channel-group 1 mode active
!
interface GigabitEthernet0/2
 description "Vers SW2 - LACP"
 switchport mode trunk
 channel-group 1 mode active
 ```

## IX. Spanning-Tree



## X. Routage InterVlan

(préférer la configuration SVI sur SW3)
